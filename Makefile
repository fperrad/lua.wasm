
LUA_RELEASE:=5.4.7
LUA_TARBALL:=lua-$(LUA_RELEASE).tar.gz
LUA_SRCDIR:=lua-$(LUA_RELEASE)/src
LUA_LIB:=$(LUA_SRCDIR)/liblua.a

CFLAGS=-O2
OPTIONS=-s EXPORTED_RUNTIME_METHODS="['ccall', 'cwrap']" \
        -s EXPORTED_FUNCTIONS="['_luaL_newstate', '_luaL_openlibs', '_luaL_loadstring', '_lua_pcallk', '_lua_tolstring', '_lua_gettop', '_lua_settop']"
EMCC=emcc
EMAR=emar rcu

.PHONY: all
all: liblua.js

.PHONY: pages
pages:
	mkdir public
	cp index.html   public/index.html
	cp liblua.js    public/liblua.js
	cp liblua.wasm  public/liblua.wasm
	cp -R res       public/res

liblua.js: $(LUA_LIB)
	$(EMCC) $(CFLAGS) $(OPTIONS) $^ -o $@

$(LUA_LIB): $(LUA_SRCDIR)
	$(MAKE) -C $< CC="$(EMCC)" AR="$(EMAR)" $(notdir $@)
	emar s $@

$(LUA_SRCDIR): $(LUA_TARBALL)
	tar xvfz $<

$(LUA_TARBALL):
	wget https://www.lua.org/ftp/$@

.PHONY: clean
clean:
	-$(MAKE) -C $(LUA_SRCDIR) clean
	rm -f liblua.js liblua.wasm

.PHONY: realclean
realclean: clean
	rm -rf lua-*
	rm -f lua-*.tar.gz
