
lua.wasm
========

See live demo on static page <https://fperrad.frama.io/lua.wasm/>.

`liblua` is compiled from unmodified sources of Lua 5.4.7 by `Emscripten` 3.1.58.

Tested with Firefox 125 on Ubuntu 64bits.
